# Contributing

## Dispatched files

You can propose a change and the files to be dispatched on your projects.

They are located on two distinct folders:

- rootfs/project: The common project files, regardless the project path.
- rootfs/nexylan: Nexylan **ONLY** common files. Will be dispatched under the Nexylan group only.

### Process

1. Edit the file you want to change. Note that some file specific indications are left on comments. Please, consider and apply the recommendation.
2. Run the `make` command to apply the file dispatch on skel itself.
3. Sometimes the `.gitlab-ci.yml` will be auto-updated. In that case, put this modification on a separated commit with the following title: `ci: update`
4. Commit and push your changes.
