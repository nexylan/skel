FROM registry.gitlab.com/nexylan/docker/core:3.1.0-debian
RUN apt-get update && apt-get install --yes --no-install-recommends \
  git=1:2.* \
  jq=1.* \
  && rm --recursive --force /var/lib/apt/lists/*
# @see https://github.com/zaquestion/lab/issues/562
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
ENV LAB_VERSION=0.23.0
RUN mkdir extracted \
  && curl -sL "https://github.com/zaquestion/lab/releases/download/v${LAB_VERSION}/lab_${LAB_VERSION}_linux_amd64.tar.gz" | tar -C extracted -xzf - \
  && install -m755 extracted/lab /usr/local/bin/lab \
  && rm -rf extracted
COPY rootfs /
WORKDIR /app
