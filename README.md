# Skel

Project skeleton with a dispatcher.

## Usage

To get your project updated, run the following commands on the root folder:

```sh
docker pull registry.gitlab.com/nexylan/skel
docker run --user=$(id --user):$(id --group) --volume=${PWD}:/app registry.gitlab.com/nexylan/skel apply
```

Then, commit your changes.

## Content

This section will add additional information about the dispatched files.

#### Common

##### .editorconfig

Editor configuration to match Nexylan coding style rules.

> EditorConfig helps maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs.

🌐 https://editorconfig.org/

#### Nexylan specific

The following files are **specific** to the Nexylan projects.

However, the documentation might fit your needs for a custom appliance.

##### CODEOWNERS

Specify which people or group to trigger an approval request.

This will ensure the MR to be approved by at least one people
who can give is expertise.

ℹ️ If you want to manage more file types for approval, please open an issue or contribute to the file! 👍

🌐 https://docs.gitlab.com/ee/user/project/code_owners.html
🌐 https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html

##### GitLab issue and merge request templates

Some default templates for issues and merge requests
are provided by the [Skel](https://gitlab.com/nexylan/skel) project.

They help author to write his request by providing him a structure to follow
and allow the maintainer to quickly identify the concern and provide the right answer to it.

It also adds some automation thanks to the
[gitlab quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html) feature.
This slightly improve issue triage time consuming.

Each template contains comments to help you filling the different sections.
Comments **won't** appear to the rendered body, you don't need to remove them each time.

They are optional but **highly recommended** and **may be required** depending the project maintainers.
Trying to use them regularly would help keeping thing clear.

Also, any contribution (as an issue or code addition) is welcomed!
The goal is to provide generic helpful templates **for any kind of project**.

They are however some simple rules:

1. The change MUST be generic. Any modification will be applied to any Nexylan project.
2. The change MAY introduce a brand new template if the first rule is guaranteed.
3. The change SHOULD NOT introduce any regression/removal to the other features (except if justified).
4. The change MUST NOT be a matter of taste, but should justify a real improvement.

Note: The change rules is also open to contribution.

If your change does not match the rules, you may want to read the following custom templates section.

###### Custom templates

Default template are here to provide basic issue tracking feature with some automation.
They are designed to fit any project configuration but may be **imprecise**.

In that case, it's recommended to create additional custom templates for specific request or report.

The process is quite simple and well described on the
[GitLab documentation](https://docs.gitlab.com/ee/user/project/description_templates.html#creating-issue-templates).

Yon can also see some examples on the [Pretty project](https://gitlab.com/nexylan/pretty/-/tree/master/.gitlab).

- ⚠️ You MUST avoid using the same file name as a one provided by nexylan/skel. It will be immediately overwritten.
- ⚠️ You SHOULD avoid using generic file name to avoid future conflicts with nexylan/skel.
  If you think your template may be useful for many projects, please submit a merge request here instead. 👍

## Features

### Release update: nexylan/ci

Updates your `.gitlab-ci.yml` file to update the `nexylan/ci` project to the latest release.

### Project settings

Note: Project settings does not apply on project not belonging to Nexylan.

You can see default project settings applied on the [related script file](./rootfs/scripts/gitlab_settings.sh).

#### Configuration

Configurable with env vars:

- `SLACK_DEV_CHANNEL`: The channel to use for issues and merge requests events. Default to `dev`.

## Development

```
make
```
