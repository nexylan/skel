#!/usr/bin/env bash
set -e

# HTTPie configuration
http_base="http --check-status --ignore-stdin"
http="${http_base} --verbose"
auth="Private-Token: ${GITLAB_TOKEN?}"

# Resources
project="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID?}"

# @see https://docs.gitlab.com/ee/api/projects.html#edit-project
# merge_method: https://youtu.be/x6vD9RHEB1M
${http} PUT "${project}" \
	wiki_access_level=disabled \
	snippets_access_level=disabled \
	pages_access_level=disabled \
	request_access_enabled:=false \
	resolve_outdated_diff_discussions:=false \
	only_allow_merge_if_pipeline_succeeds:=true \
	only_allow_merge_if_all_discussions_are_resolved:=true \
	merge_method=merge \
	remove_source_branch_after_merge:=true \
	auto_cancel_pending_pipelines=enabled \
	"${auth}"

# @see https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-configuration
${http} POST "${project}"/approvals \
	approvals_before_merge:=1 \
	reset_approvals_on_push:=true \
	merge_requests_author_approval:=false \
	merge_requests_disable_committers_approval:=false \
	reset_approvals_on_push:=true \
	"${auth}"

# @see https://docs.gitlab.com/ee/api/protected_tags.html#protect-repository-tags
${http} DELETE "${project}"/protected_tags/'v*' "${auth}" || true
${http} POST "${project}"/protected_tags \
	name='v*' \
	create_access_level=40 \
	"${auth}"

# @see https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches
${http} DELETE "${project}"/protected_branches/"${CI_DEFAULT_BRANCH}" "${auth}" || true
${http} POST "${project}"/protected_branches \
	name="${CI_DEFAULT_BRANCH}" \
	push_access_level=0 \
	merge_access_level=30 \
	unprotect_access_level=40 \
	code_owner_approval_required:=true \
	"${auth}"

# @see https://docs.gitlab.com/ee/api/pipeline_schedules.html
pipeline_id=$(
	${http_base} GET "${project}"/pipeline_schedules \
		"${auth}" |
		jq --raw-output '.[] | .id, .description' |
		grep --before-context=1 maintain |
		head --lines=1
)
if [ -z "${pipeline_id}" ]; then
	pipeline_update="${http} POST ${project}/pipeline_schedules"
else
	pipeline_update="${http} PUT ${project}/pipeline_schedules/${pipeline_id}"
fi
${pipeline_update} \
	description=maintain \
	ref="${CI_DEFAULT_BRANCH}" \
	cron='0 8 * * 0' \
	cron_timezone='Europe/Paris' \
	active:=true \
	"${auth}"

${http} POST "${project}"/pipeline_schedules/"${pipeline_id}"/take_ownership \
	"${auth}"

disable_stages=(TEST DEPLOY RELEASE VERIFY)
for stage in "${disable_stages[@]}"; do
	pipeline_env_var="NEXY_CI_STAGE_${stage}"

	${http} DELETE "${project}"/pipeline_schedules/"${pipeline_id}"/variables/"${pipeline_env_var}" "${auth}" || true
	${http} POST "${project}"/pipeline_schedules/"${pipeline_id}"/variables/ \
		key="${pipeline_env_var}" \
		value=0 \
		"${auth}"
done
